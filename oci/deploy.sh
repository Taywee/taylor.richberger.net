#!/bin/sh
set -euf

umask 0077
mkdir -p ~/.ssh
echo "$PLUGIN_ID_RSA" >~/.ssh/id_rsa
echo "$PLUGIN_KNOWN_HOSTS" >~/.ssh/known_hosts
chmod -R go-rwx ~/.ssh

echo "${PLUGIN_RSYNC:-[]}" | jq -c '.[]' | while read sync; do
  source=$(echo "$sync" | jq -r '.source')
  destination=$(echo "$sync" | jq -r '.destination')
  delete=$(echo "$sync" | jq -r '.delete')
  echo "$source -> $destination"
  if [ 'true' = "$delete" ]; then
    rsync --mkpath --delete -av "$source" "${PLUGIN_REMOTE}":"$destination"
  else
    rsync --mkpath -av "$source" "${PLUGIN_REMOTE}":"$destination"
  fi
done

if [ -n "${PLUGIN_SCRIPT:-}" ]; then
  ssh ${PLUGIN_REMOTE} "
set -ex
$PLUGIN_SCRIPT
"
fi
