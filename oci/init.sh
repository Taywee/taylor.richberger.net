#!/bin/sh

set -eux

/opt/axfive/venv/bin/python -mdjango migrate --no-input
/opt/axfive/venv/bin/python -mdjango update_index
