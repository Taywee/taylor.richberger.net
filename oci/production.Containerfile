FROM docker.io/python:3.12-slim-bookworm AS build

ENV PYTHONUNBUFFERED=1 \
    DJANGO_SETTINGS_MODULE=axfive.axfive.settings.production \
    STATIC_ROOT=/opt/axfive/files/static \
    MEDIA_ROOT=/opt/axfive/files/media \
    STATICFILES_DIRS=/opt/axfive/staticgen

WORKDIR /opt/axfive/app

RUN \
    --mount=type=cache,target=/var/cache/apt,sharing=locked \
    --mount=type=cache,target=/var/lib/apt,sharing=locked \
    set -euxf; \
    apt-get update --yes --quiet; \
    apt-get install --yes --quiet --no-install-recommends \
    build-essential \
    libpq-dev \
    python3-build \
    python3-pip \
    python3-venv \
    python3-wheel \
    ;

COPY src src
COPY pyproject.toml README.md .

RUN set -euxf; \
    python3 -mvenv --upgrade-deps --system-site-packages /opt/axfive/venv; \
    /opt/axfive/venv/bin/pip install flit; \
    /opt/axfive/venv/bin/flit build; \
    /opt/axfive/venv/bin/pip install --find-links file:///opt/axfive/app/dist 'axfive[production]' 'gunicorn[gevent]'; \
    mkdir -p /opt/axfive/staticgen; \
    SECRET_KEY=temporary /opt/axfive/venv/bin/django-admin generate_pygments_css -o /opt/axfive/staticgen/css/code.css; \
    /opt/axfive/venv/bin/django-admin collectstatic --no-input --clear

FROM docker.io/python:3.12-slim-bookworm AS webapp

ENV PYTHONUNBUFFERED=1 \
    PORT=8000 \
    DJANGO_SETTINGS_MODULE=axfive.axfive.settings.production \
    STATIC_ROOT=/opt/axfive/files/static \
    MEDIA_ROOT=/opt/axfive/files/media

RUN \
    --mount=type=cache,target=/var/cache/apt,sharing=locked \
    --mount=type=cache,target=/var/lib/apt,sharing=locked \
    set -euxf; \
    apt-get update --yes --quiet; \
    apt-get install --yes --quiet --no-install-recommends \
    libjpeg62-turbo \
    libpq-dev \
    libwebp7 \
    python3 \
    tini \
    zlib1g \
    ;

COPY --from=build /opt/axfive/files/static/staticfiles.json /opt/axfive/files/static/staticfiles.json
COPY --from=build /opt/axfive/venv /opt/axfive/venv/
COPY ./oci/init.sh ./oci/run.sh /opt/axfive/bin/

RUN set -euxf; \
    useradd -MUr axfive; \
    mkdir -p /opt/axfive/files/media; \
    chown axfive:axfive /opt/axfive/files/media

VOLUME /opt/axfive/files/media

USER axfive

EXPOSE 8000

ENTRYPOINT ["/bin/tini", "--"]
CMD ["/opt/axfive/bin/run.sh"]

FROM docker.io/caddy:latest AS fileserver

COPY --from=build /opt/axfive/files/static /opt/axfive/files/static/

# STATICFILES.json is not needed from caddy.
RUN rm /opt/axfive/files/static/staticfiles.json

COPY ./robots.txt /opt/axfive/files/

VOLUME /opt/axfive/files/media

EXPOSE 8001

CMD ["caddy", "file-server", "--root", "/opt/axfive/files", "--listen", ":8001"]

