#!/bin/sh

bin=/opt/axfive/venv/bin
pip="$bin/pip"
python="$bin/python"

set -eux

"$pip" install -e '.[dev]' >&2

if [ $# -eq 0 ]; then
  set -eux

  manage() {
    "$python" ./manage.py "$@"
  }

  manage migrate --no-input

  if [ -e "$PWD/fixtures/development.json" -a ! -e /var/opt/axfive/fixtures_loaded ]; then
    manage loaddata "$PWD/fixtures/development.json"
    touch /var/opt/axfive/fixtures_loaded
  fi

  manage update_index

  manage generate_pygments_css -o /var/opt/axfive/static/css/code.css

  manage runserver 0.0.0.0:8000
else
  exec "$@"
fi
