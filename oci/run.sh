#!/bin/sh

set -eux

# gunicorn is not publicly accessible; can accept all forwards.
exec /opt/axfive/venv/bin/gunicorn --worker-class gevent --forwarded-allow-ips='*'  --access-logfile - axfive.axfive.wsgi:application
