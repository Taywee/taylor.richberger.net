"""Home app config."""
from django.apps import AppConfig


class HomeConfig(AppConfig):
    """Home app config."""

    default_auto_field = "django.db.models.BigAutoField"
    name = "axfive.home"
