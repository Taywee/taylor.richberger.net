"""Simple pages without nested responsibility."""
from axfive.article.blocks import BodyBlock
from django.db import models
from django.utils.translation import gettext_lazy as _
from wagtail.admin.panels import FieldPanel
from wagtail.fields import StreamField
from wagtail.models import Page


class HomePage(Page):
    """Base home page model."""

    menu_title = models.CharField(
        verbose_name=_("menu title"),
        max_length=255,
        help_text=_("The title that appears in menus"),
    )

    content_panels = (
        *Page.content_panels,
        FieldPanel("menu_title"),
    )

    parent_page_types = ()

class CodeHomePage(HomePage):
    """Home page for a programming website.

    This one has a body, because it is expected to have some sort of
    introduction.
    """

    body = StreamField(BodyBlock(), use_json_field=True)

    content_panels = (
        *HomePage.content_panels,
        FieldPanel("body"),
    )

    parent_page_types = (
        "wagtailcore.Page",
    )


class ArtHomePage(HomePage):
    """Home page for an art website."""

    parent_page_types = (
        "wagtailcore.Page",
    )
