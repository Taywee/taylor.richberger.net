"""Blog models."""

from axfive.article.models import Article
from django.db import models
from django.utils import timezone
from wagtail.admin.panels import FieldPanel
from wagtail.models import Page
from wagtail.search.index import SearchField


class BlogIndexPage(Page):
    """A container section for blog posts."""

    parent_page_types = (
        "home.ArtHomePage",
        "home.CodeHomePage",
    )

class BlogPage(Article):
    """A single blog post."""

    date = models.DateField("Post date", default=timezone.now)
    summary = models.TextField(max_length=450)

    search_fields = (
        *Article.search_fields,
        SearchField("summary"),
    )

    content_panels = (
        FieldPanel("date"),
        *Article.content_panels,
    )

    promote_panels = (
        *Article.promote_panels,
        FieldPanel("summary"),
    )

    parent_page_types = (
        "blog.BlogIndexPage",
    )
