"""The base app defining models for blog types.

These models aren't independent.  They are subclassed for a project blog, a
journal, etc.

They may end up being all just exactly like the root blog, but at least we can
filter by type if we need to.
"""
