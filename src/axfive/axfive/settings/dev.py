"""Development settings."""

from os import environ
from pathlib import Path

from . import *  # noqa: F403
from . import INSTALLED_APPS, LOGGING

DEBUG = True

SECRET_KEY = "django-insecure-abmp&u$_r)4d9zj(c$*to2&(xm+!d_a*xxjxw$t4(-cf46+@vi"

ALLOWED_HOSTS = ["*"]

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

database_file = Path("/var/opt/axfive/db.sqlite3")

if "DATABASE_FILE" in environ:
    database_file = Path(environ["DATABASE_FILE"])

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": database_file,
    },
}

WAGTAILSEARCH_BACKENDS = {
    "default": {
        "BACKEND": "wagtail.search.backends.database",
    },
}

LOGGING["root"]["level"] = "DEBUG"

INSTALLED_APPS.append("django_extensions")
