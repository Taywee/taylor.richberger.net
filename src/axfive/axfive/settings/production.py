"""Production settings."""

from os import environ

from . import *  # noqa: F403
from ._util import get_secret, map_opt

DEBUG = False

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": environ.get("DATABASE_NAME", "axfive"),
        "USER": environ.get("DATABASE_USER", "axfive"),
        "PASSWORD": map_opt(get_secret("database password"), lambda s: s.rstrip("\r\n")),
        "HOST": environ.get("DATABASE_HOST", "localhost"),
        "PORT": int(environ.get("DATABASE_PORT", 5432)),
    },
}

SECRET_KEY = map_opt(get_secret("secret key"), lambda s: s.rstrip("\r\n"))
ALLOWED_HOSTS = (
    "axfive.net",
    "axfive.test",
    "taywee.art",
    "taywee.test",
)
CSRF_TRUSTED_ORIGINS = (
    "https://axfive.net",
    "https://axfive.test",
    "https://taywee.art",
    "https://taywee.test",
)
