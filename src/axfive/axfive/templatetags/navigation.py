"""Tags for menus."""

from __future__ import annotations

from itertools import chain
from typing import TYPE_CHECKING, Iterable

from django import template

if TYPE_CHECKING:
    from wagtail.models import Page, Site

register = template.Library()

@register.simple_tag
def menu_pages(site: Site | None) -> Iterable[Page]:
    """Generate menu pages from a site."""
    pagesets: list[Iterable[Page]] = []
    if site and (root := site.root_page):
        if root.show_in_menus:
            pagesets.append((root,))
        pagesets.append(root.get_children().live().in_menu())
    return chain.from_iterable(pagesets)

@register.simple_tag
def menu_title(page: Page) -> str:
    menu_title = getattr(page.specific, "menu_title", None)
    return menu_title or page.title
