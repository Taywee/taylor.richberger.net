"""Core app models."""

from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    """The user class for the axfive app.

    This currently is just an AbstractUser.
    """
