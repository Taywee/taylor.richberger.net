"""Axfive app config."""
from django.apps import AppConfig


class AxfiveConfig(AppConfig):
    """Axfive app config."""

    default_auto_field = "django.db.models.BigAutoField"
    name = "axfive.axfive"
