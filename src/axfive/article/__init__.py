"""The base article app.

This has inheritable models for pages of mixed content and prose.

This is the base for any page that presents a main block of body content.
"""
