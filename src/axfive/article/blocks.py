"""Article app blocks."""

from functools import lru_cache

from django.utils.safestring import mark_safe
from pygments import highlight, lexers
from pygments.formatters import HtmlFormatter
from pygments.lexer import Lexer
from wagtail import blocks
from wagtail.images.blocks import ImageChooserBlock

_FORMATTER = HtmlFormatter(linenos=False, cssclass="code")

def _lexer(language, file_name, body) -> Lexer:
    if language:
        return lexers.get_lexer_by_name(language)

    if file_name:
        return lexers.get_lexer_for_filename(file_name, body)

    return lexers.guess_lexer(body)

@lru_cache
@mark_safe
def _highlight_code(language, file_name, body) -> str:
    lexer = _lexer(language, file_name, body)
    return highlight(body, lexer, _FORMATTER)


class CodeValue(blocks.StructValue):
    """The value type for CodeBlock, to enable dynamically pulling the highlighted body."""

    @property
    def file_name(self) -> str:
        """File name."""
        return self["file_name"]

    @property
    def language(self) -> str:
        """Language."""
        return self["language"]

    @property
    def body(self) -> str:
        """Body."""
        return self["body"]

    @property
    def highlighted(self) -> str:
        """The code highlighted based on its filename, language, or a guess."""
        return _highlight_code(
            language=self.language,
            file_name=self.file_name,
            body=self.body,
        )

class CodeBlock(blocks.StructBlock):
    """A block for inline syntax-highlighted code."""

    file_name = blocks.CharBlock(required=False)
    language = blocks.CharBlock(required=False)
    body = blocks.TextBlock()

    class Meta:
        """Django metaclass."""

        icon = "code"
        template = "article/blocks/code.html"
        value_class = CodeValue

class BaseBodyBlock(blocks.StreamBlock):
    """Body block base, inherited by specific block types.."""

    rich_text = blocks.RichTextBlock(
        features=[
            "blockquote",
            "bold",
            "code",
            "hr",
            "italic",
            "link",
            "ol",
            "strikethrough",
            "subscript",
            "superscript",
            "ul",
        ],
    )
    image = ImageChooserBlock()
    code = CodeBlock()

class SubSubSectionBodyBlock(BaseBodyBlock):
    """The body block for a SubSubSectionBlock."""

class SubSubSectionBlock(blocks.StructBlock):
    """A SubSubSection.

    The same thing as a section, but nested in a SubSection.
    """

    title = blocks.CharBlock(form_classname="title")
    body = SubSubSectionBodyBlock()

    class Meta:
        """Django metaclass."""

        template = "article/blocks/subsubsection.html"

class SubSectionBodyBlock(BaseBodyBlock):
    """The body block for a SubSectionBlock."""

    section = SubSubSectionBlock()

class SubSectionBlock(blocks.StructBlock):
    """A SubSection.

    The same thing as a section, but nested in a Section.
    """

    title = blocks.CharBlock(form_classname="title")
    body = SubSectionBodyBlock()

    class Meta:
        """Django metaclass."""

        template = "article/blocks/subsection.html"

class SectionBodyBlock(BaseBodyBlock):
    """The body block for a SectionBlock."""

    section = SubSectionBlock()

class SectionBlock(blocks.StructBlock):
    """A section nested in the article.

    For a titled section inside the article.
    """

    title = blocks.CharBlock(form_classname="title")
    body = SectionBodyBlock()

    class Meta:
        """Django metaclass."""

        template = "article/blocks/section.html"

class BodyBlock(BaseBodyBlock):
    """The body block for the entire article."""

    section = SectionBlock()
