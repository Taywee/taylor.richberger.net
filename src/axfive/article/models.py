"""Article app models."""
from wagtail.admin.panels import FieldPanel
from wagtail.fields import StreamField
from wagtail.models import Page
from wagtail.search.index import SearchField

from .blocks import BodyBlock


class Article(Page):
    """A generic article model, with a streamfield of arbitrary sections."""

    body = StreamField(
        BodyBlock(),
        use_json_field=True,
    )

    search_fields = (
        *Page.search_fields,
        SearchField("body"),
    )

    content_panels = (
        *Page.content_panels,
        FieldPanel("body"),
    )

    parent_page_types = ()
