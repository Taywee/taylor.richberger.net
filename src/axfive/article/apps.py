"""Article app config."""
from django.apps import AppConfig


class ArticleConfig(AppConfig):
    """Article app config."""

    default_auto_field = "django.db.models.BigAutoField"
    name = "axfive.article"
