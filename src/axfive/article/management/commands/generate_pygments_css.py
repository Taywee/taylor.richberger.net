"""Generate pygments css declarations."""

from argparse import ArgumentParser
from logging import getLogger
from pathlib import Path

from django.core.management.base import BaseCommand
from pygments.formatters import HtmlFormatter

logger = getLogger(__name__)

DARK_FORMATTER = HtmlFormatter(linenos=True, cssclass="code", style="dracula")
LIGHT_FORMATTER = HtmlFormatter(linenos=True, cssclass="code", style="default")
class Command(BaseCommand):
    """Generate pygments css declarations."""

    def add_arguments(self, parser: ArgumentParser) -> None:
        """Add options and arguments to the parser."""
        parser.add_argument("-o", "--output", required=True, type=Path, help="The destination")

    def handle(self, *args, **options) -> None:
        """Run the pygments generator."""
        output: Path = options["output"]
        output.parent.mkdir(parents=True, exist_ok=True)
        logger.info("generating css into %s", output)
        with output.open("w") as file:
            # Note that the light color scheme inherits the text color, so  the
            # dark color scheme can clobber it and give you white text on a
            # white background.  This isn't a problem now, but something to keep
            # in mind when implementing theme override.
            file.write("@media (prefers-color-scheme: dark) {\n")
            file.write(DARK_FORMATTER.get_style_defs(".code"))
            file.write("\n}\n@media (prefers-color-scheme: light) {\n")
            file.write(LIGHT_FORMATTER.get_style_defs(".code"))
            file.write("\n}\n")
