"""Simple pages without nested responsibility."""
from axfive.article.models import Article


class SimplePage(Article):
    """A simple exension of an article.

    This is just a single informational page, intended to be top-level.
    """

    # Currently doesn't define any extra fields.

    parent_page_types = (
        "home.HomePage",
    )

