"""Simple app config."""
from django.apps import AppConfig


class SimpleConfig(AppConfig):
    """Simple app config."""

    default_auto_field = "django.db.models.BigAutoField"
    name = "axfive.simple"
