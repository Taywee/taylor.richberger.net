---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: media
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 5Gi
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: app
  labels:
    app: app
spec:
  selector:
    matchLabels:
      app: app
  template:
    metadata:
      labels:
        app: app
    spec:
      initContainers:
        - name: wait-postgres
          image: docker.io/postgres:latest
          resources:
            requests:
              memory: 64Mi
            limits:
              memory: 512Mi
          args:
            - /bin/sh
            - -c
            - |
              i=0
              total=300
              while [ $((i += 1)) -lt $total ]; do
                pg_isready -U axfive -h postgres && exit 0
                echo "Waiting for postgres to start listening ($i/$total)"
                sleep 1
              done
              echo 'could not start postgres' >&2
              exit 1
        - name: chown-media
          image: docker.io/debian:bookworm-slim
          resources:
            requests:
              memory: 128Mi
            limits:
              memory: 512Mi
          volumeMounts:
            - name: media
              mountPath: /opt/axfive/files/media
          args:
            - /bin/sh
            - -c
            - |
              chown 999:999 /opt/axfive/files/media
        - name: init
          image: docker.io/taywee/axfive.net-webapp:latest
          resources:
            requests:
              memory: 128Mi
            limits:
              memory: 512Mi
          volumeMounts:
            - name: media
              mountPath: /opt/axfive/files/media
            - name: postgres-secret
              readOnly: true
              mountPath: /run/secrets/postgres
            - name: app-secret
              readOnly: true
              mountPath: /run/secrets/app
          env:
            - name: DATABASE_HOST
              value: postgres
            - name: DATABASE_PASSWORD_FILE
              value: /run/secrets/postgres/password
            - name: SECRET_KEY_FILE
              value: /run/secrets/app/secret-key
          args:
            - /bin/sh
            - /opt/axfive/bin/init.sh
      containers:
        - name: django
          image: docker.io/taywee/axfive.net-webapp:latest
          resources:
            requests:
              memory: 128Mi
            limits:
              memory: 512Mi
          startupProbe:
            httpGet:
              path: /
              port: 8000
              httpHeaders:
                - name: Host
                  value: axfive.net
            failureThreshold: 100
            periodSeconds: 1
          readinessProbe:
            httpGet:
              path: /
              port: 8000
              httpHeaders:
                - name: Host
                  value: axfive.net
            failureThreshold: 3
            periodSeconds: 10
          livenessProbe:
            httpGet:
              path: /
              port: 8000
              httpHeaders:
                - name: Host
                  value: axfive.net
            failureThreshold: 10
            periodSeconds: 10
          volumeMounts:
            - name: media
              mountPath: /opt/axfive/files/media
            - name: postgres-secret
              readOnly: true
              mountPath: /run/secrets/postgres
            - name: app-secret
              readOnly: true
              mountPath: /run/secrets/app
          env:
            - name: DATABASE_HOST
              value: postgres
            - name: DATABASE_PASSWORD_FILE
              value: /run/secrets/postgres/password
            - name: SECRET_KEY_FILE
              value: /run/secrets/app/secret-key
            - name: WAGTAIL_ADMIN_PATH_FILE
              value: /run/secrets/app/wagtail-admin-path
            - name: DJANGO_ADMIN_PATH_FILE
              value: /run/secrets/app/django-admin-path
          ports:
            - containerPort: 8000
              name: app
        - name: files
          image: docker.io/taywee/axfive.net-fileserver:latest
          resources:
            requests:
              memory: 64Mi
            limits:
              memory: 256Mi
          startupProbe:
            httpGet:
              path: /robots.txt
              port: 8001
            failureThreshold: 100
            periodSeconds: 1
          readinessProbe:
            httpGet:
              path: /robots.txt
              port: 8001
            failureThreshold: 3
            periodSeconds: 10
          livenessProbe:
            httpGet:
              path: /robots.txt
              port: 8001
            failureThreshold: 10
            periodSeconds: 10
          volumeMounts:
            - name: media
              mountPath: /opt/axfive/files/media
              readOnly: true
          ports:
            - containerPort: 8001
              name: files
      volumes:
        - name: media
          persistentVolumeClaim:
            claimName: media
        - name: postgres-secret
          secret:
            secretName: postgres
        - name: app-secret
          secret:
            secretName: app
---
apiVersion: v1
kind: Service
metadata:
  name: app
spec:
  selector:
    app: app
  ports:
    - name: http
      protocol: TCP
      appProtocol: http
      port: 80
      targetPort: app
---
apiVersion: v1
kind: Service
metadata:
  name: app-files
spec:
  selector:
    app: app
  ports:
    - name: http
      protocol: TCP
      appProtocol: http
      port: 80
      targetPort: files
