docker := env_var_or_default('DOCKER', 'docker')
image-version := 'latest'

_default:
	@just --list

_network:
	{{docker}} network inspect axfive >/dev/null 2>&1 || \
		{{docker}} network create axfive

# Build the development image
dev-image:
	{{docker}} image build -f oci/dev.Containerfile -t localhost/dev.axfive.net .

# Build production-images
images: (production-image 'webapp') (production-image 'fileserver')

# Build the stage of the production image
production-image stage:
	{{docker}} image build --target {{stage}} -f oci/production.Containerfile -t docker.io/taywee/axfive.net-{{stage}}:{{image-version}} .

push name:
	{{docker}} image push docker.io/taywee/axfive.net-{{name}}:{{image-version}}

push-images: (push 'webapp') (push 'fileserver')

push-deploy:
	{{docker}} image push docker.io/taywee/deploy

# Run the development container
dev:
	{{docker}} container run \
		-it \
		--rm \
		--name axfive-dev \
		--mount type=volume,source=axfive-dev-venv,destination=/opt/axfive/venv \
		--mount type=volume,source=axfive-dev-var,destination=/var/opt/axfive \
		--mount type=bind,source={{justfile_directory()}},destination=/opt/axfive/app \
		--security-opt label=disable \
		-p 8000:8000 \
		localhost/dev.axfive.net

set positional-arguments

# Run a django-admin command in the development container
django *args:
	{{docker}} container run \
		-it \
		--rm \
		--mount type=volume,source=axfive-dev-venv,destination=/opt/axfive/venv \
		--mount type=volume,source=axfive-dev-var,destination=/var/opt/axfive \
		--mount type=bind,source={{justfile_directory()}},destination=/opt/axfive/app \
		--security-opt label=disable \
		localhost/dev.axfive.net \
		/opt/axfive/app/oci/dev.sh /opt/axfive/venv/bin/django-admin "$@"

# Make development fixtures
development-fixtures:
	{{docker}} container run \
		-it \
		--rm \
		--mount type=volume,source=axfive-dev-venv,destination=/opt/axfive/venv \
		--mount type=volume,source=axfive-dev-var,destination=/var/opt/axfive \
		--mount type=bind,source={{justfile_directory()}},destination=/opt/axfive/app \
		--security-opt label=disable \
		localhost/dev.axfive.net \
		/opt/axfive/app/oci/dev.sh \
		/opt/axfive/venv/bin/django-admin \
		dumpdata \
		--natural-foreign \
		--natural-primary \
		-e wagtailsearch \
		--indent 2 > ./fixtures/development.json

# Format all files.
format:
	{{docker}} container run \
		-it \
		--rm \
		--mount type=volume,source=axfive-dev-venv,destination=/opt/axfive/venv \
		--mount type=volume,source=axfive-dev-var,destination=/var/opt/axfive \
		--mount type=bind,source={{justfile_directory()}},destination=/opt/axfive/app \
		--security-opt label=disable \
		localhost/dev.axfive.net \
		/opt/axfive/app/oci/dev.sh \
		/opt/axfive/venv/bin/ruff \
		check \
		--fix \
		./src

clean:
	-{{docker}} container rm -f axfive-dev
	-{{docker}} volume rm -f axfive-dev-venv
	-{{docker}} volume rm -f axfive-dev-var
	-{{docker}} network rm -f axfive
